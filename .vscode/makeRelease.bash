#!/usr/bin/env bash

releaseDir="releases"

# If release dir not exist
if [ ! -d "$releaseDir" ]; then
    mkdir $releaseDir
fi

repoDir=$1
tempDir=$(mktemp -d)

cp -r $repoDir/* $tempDir # Copy all the files there

# REMOVE unused files
rm -f $tempDir/.git
rm -f $tempDir/README.md

# source $tempDir/module.prop # read the module props
releaseName=$(basename ${repoDir})
releaseName="${releaseName}.zip"
# releaseName="${releaseName}-${version}.zip"

(
    cd $tempDir
    zip $releaseName -r *
)

cp $tempDir/$releaseName $releaseDir/$releaseName
rm -rf $tempDir
