#!/usr/bin/env python3
import sys
import tempfile
from os import path
import re

# Original file
fileLocation = sys.argv[1]
filename = path.basename(fileLocation)
file = open(fileLocation, "r")
# Temp file
tmpDir = tempfile.gettempdir()
tmpFileLocation = path.join(tmpDir, filename)
tmpFile = open(tmpFileLocation, "w")
# Multiply factor
factor = float(sys.argv[2])
if factor >= 1.0:
    factor = int(factor)

# File exploration line by line
lineFound = False
for line in file:
    if lineFound:
        lineFound = False
        searchResults = re.match("(.*)bitRate=\"(.*)\"", line)
        if searchResults:
            bitrateValue = int(searchResults.group(2))
            bitrateValue /= 100
            bitrateValue *= factor
            line = searchResults.group(
                1) + "bitRate=\"" + str(int(bitrateValue)) + "00\"\n"
            tmpFile.write(line)
        else:
            tmpFile.write(line)
    else:
        tmpFile.write(line)
        searchResults = re.match(" *.<Video codec=\"hevc\"", line)
        if searchResults:
            lineFound = True

# Remove and replace old file
tmpFile = open(tmpFileLocation, "r")
file = open(fileLocation, "w")

for line in tmpFile:
    file.write(line)
