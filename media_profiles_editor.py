#!/usr/bin/env python3
import tempfile
import xml.etree.cElementTree as et
import sys
from os import path
import copy


class AttributeItem:
    """
        Model for parsing attribute list items
    """
    Type: str
    Parameter: str
    Values: list
    Required: bool
    Cdata: bool
    __begin = '<!ATTLIST'
    __separator = '|'
    __empty = 'EMPTY'
    __required = '#REQUIRED'
    __end = '>\n'

    def __init__(self, Type: str = '', Parameter: str = '', Values: list = [], Required: bool = False, Cdata: bool = False):
        self.Type = Type
        self.Parameter = Parameter
        self.Values = Values
        self.Required = Required
        self.Cdata = Cdata
        pass

    def toXMLString(self):
        XMLString = self._AttributeItem__begin + ' '
        XMLString += self.Type + ' '
        XMLString += self.Parameter + ' '
        if self.Values:
            if not self.Cdata:
                XMLString += '('
                for value in self.Values:
                    XMLString += value + self._AttributeItem__separator
                XMLString = XMLString[:len(XMLString) - 1] + ') '
            else:
                XMLString += self.Values[0] + ' '
        else:
            XMLString += self._AttributeItem__empty
        if self.Required:
            XMLString += self._AttributeItem__required
        XMLString += self._AttributeItem__end
        return XMLString

    def parseFromString(self, string: str):
        wordList = string.split(' ')
        if wordList[0] != self._AttributeItem__begin:  # If it isn't an AttributeItem
            return False
        self.Type = wordList[1]
        self.Parameter = wordList[2]
        values = wordList[3]
        if '(' in values:
            values = values[1:len(values) - 1]  # Remove brackets
            self.Cdata = False
        else:
            self.Cdata = True
        self.Values = values.split(self._AttributeItem__separator)
        self.Required = bool(wordList[4])
        return True


def editAttributeList(originalFile: str,
                      videoCodecsToAdd: list = ['hevc'], audioCodecsToAdd: list = ['heaac']):
    """
        Edit the attribute list adding declarations for audio and video codecs, return the edited file lines
    """
    # Check values
    types = ['Video', 'Audio', 'VideoEncoderCap',
             'AudioEncoderCap', 'ExportVideoProfile']
    parameters = ['codec', 'name']
    lines = []
    for line in open(originalFile, 'r'):
        if line.startswith('<MediaSettings>'):  # Interruption for the XML begin
            break
        attList = AttributeItem()
        if attList.parseFromString(line):  # If it were successful
            if attList.Type in types and attList.Parameter in parameters:
                if 'Video' in attList.Type:
                    for codec in videoCodecsToAdd:
                        if codec not in attList.Values:
                            attList.Values.append(codec)
                else:
                    for codec in audioCodecsToAdd:
                        if codec not in attList.Values:
                            attList.Values.append(codec)
            lines.append(attList.toXMLString())
        else:
            lines.append(line)
    return lines


def mediaProfilesPrint(tree: et.ElementTree,
                       attributeList: list, outputFile: str = 'media_profiles_vendor.xml'):
    """
        Print the media profiles file to a new file, require the attribute list
    """
    # tmpfile
    tmpDir = tempfile.gettempdir()
    tmpFileName = path.basename(outputFile)
    tmpFileLocation = path.join(tmpDir, tmpFileName)
    tree.write(tmpFileLocation, 'UTF-8', False)
    # Copy of the complete file
    out = open(outputFile, 'w')
    for line in attributeList:
        out.write(line)
    for line in open(tmpFileLocation, 'r'):
        out.write(line)


def addExportVideoProfile(MediaSettings: et.Element,
                          basedOnVideoCodec: str = 'h264', videoCodec: str = 'hevc'):
    """
        Create an ExportVideoProfile tag for a videoCodec
    """
    # VideoEncoderCap
    newExportVideoProfile = MediaSettings.find(
        "ExportVideoProfile[@name='" + videoCodec + "']")
    baseExportVideoProfile = MediaSettings.find(
        "ExportVideoProfile[@name='" + basedOnVideoCodec + "']")
    if newExportVideoProfile != None:
        newExportVideoProfile.attrib = baseExportVideoProfile.attrib.copy()
        newExportVideoProfile.attrib['name'] = videoCodec
    else:
        newExportVideoProfile = copy.deepcopy(baseExportVideoProfile)
        newExportVideoProfile.attrib['name'] = videoCodec
        MediaSettings.append(newExportVideoProfile)


def addEncoderCap(MediaSettings: et.Element, videoBitrateModifier: float = 1.0, audioBitrateModifier: float = 1.0,
                  basedOnVideoCodec: str = 'h264', basedOnAudioCodec: str = 'aac',
                  videoCodec: str = 'hevc', audioCodec: str = 'heaac'):
    """
        Create encoder caps for video and audio based on already existing encoder caps
    """
    # VideoEncoderCap
    newEncoderCap = MediaSettings.find(
        "VideoEncoderCap[@name='" + videoCodec + "']")
    baseEncoderCap = MediaSettings.find(
        "VideoEncoderCap[@name='" + basedOnVideoCodec + "']")
    if newEncoderCap != None:
        newEncoderCap.attrib = baseEncoderCap.attrib.copy()
        newEncoderCap.attrib['name'] = videoCodec
    else:
        newEncoderCap = copy.deepcopy(baseEncoderCap)
        newEncoderCap.attrib['name'] = videoCodec
        MediaSettings.append(newEncoderCap)

    # Adjust Video maxBitRate
    newMaxBitRate = float(newEncoderCap.attrib.get('maxBitRate')[:5])
    newMaxBitRate *= videoBitrateModifier
    newMaxBitRate = str(int(newMaxBitRate)) + \
        newEncoderCap.attrib.get('maxBitRate')[5:]
    newEncoderCap.attrib['maxBitRate'] = newMaxBitRate
    # Adjust Video minBitRate
    # newMinBitRate = float(newEncoderCap.attrib.get('minBitRate')[:5])
    # newMinBitRate *= videoBitrateModifier
    # newMinBitRate = str(int(newMinBitRate)) + \
    #     newEncoderCap.attrib.get('minBitRate')[5:]
    # newEncoderCap.attrib['minBitRate'] = newMinBitRate

    # AudioEncoderCap
    newEncoderCap = MediaSettings.find(
        "AudioEncoderCap[@name='" + audioCodec + "']")
    baseEncoderCap = MediaSettings.find(
        "AudioEncoderCap[@name='" + basedOnAudioCodec + "']")
    if newEncoderCap != None:
        newEncoderCap.attrib = baseEncoderCap.attrib.copy()
        newEncoderCap.attrib['name'] = audioCodec
    else:
        newEncoderCap = copy.deepcopy(baseEncoderCap)
        newEncoderCap.attrib['name'] = audioCodec
        MediaSettings.append(newEncoderCap)

    # Adjust Audio maxBitRate
    newMaxBitRate = float(newEncoderCap.attrib.get('maxBitRate')[:5])
    newMaxBitRate *= audioBitRateModifier
    newMaxBitRate = str(int(newMaxBitRate)) + \
        newEncoderCap.attrib.get('maxBitRate')[5:]
    newEncoderCap.attrib['maxBitRate'] = newMaxBitRate

    # Export video profile
    addExportVideoProfile(MediaSettings, basedOnVideoCodec, videoCodec)


def changeCodecs(MediaSettings: et.Element, videoBitrateModifier: float = 1.0, audioBitrateModifier: float = 1.0,
                 searchedVideoCodec: str = 'h264', searchedAudioCodec: str = 'aac',
                 videoCodec: str = 'hevc', audioCodec: str = 'heaac'):
    """
        Change audio and video codecs in Camcorder profiles
    """
    for Profile in MediaSettings:
        if Profile.tag == 'CamcorderProfiles':
            for EncoderProfile in Profile:
                if EncoderProfile.tag == 'EncoderProfile':
                    Video = EncoderProfile.find('Video')
                    Audio = EncoderProfile.find('Audio')
                    if Video.attrib.get('codec') == searchedVideoCodec:
                        # Video codec edit
                        Video.attrib['codec'] = videoCodec
                        # Video bitrate edit
                        newBitrate = float(Video.attrib.get('bitRate')[:5])
                        newBitrate *= videoBitrateModifier
                        newBitrate = str(int(newBitrate)) + \
                            Video.attrib.get('bitRate')[5:]
                        Video.attrib['bitRate'] = newBitrate

                        # If audio codec it's found
                        if Audio.attrib.get('codec') == searchedAudioCodec:
                            # Audio codec edit
                            Audio.attrib['codec'] = audioCodec
                            # Audio bitrate edit
                            newBitrate = float(Audio.attrib.get('bitRate')[:5])
                            newBitrate *= audioBitRateModifier
                            newBitrate = str(int(newBitrate)) + \
                                Audio.attrib.get('bitRate')[5:]
                            Audio.attrib['bitRate'] = newBitrate

    addEncoderCap(MediaSettings, videoBitrateModifier, audioBitRateModifier, searchedVideoCodec,
                  searchedAudioCodec, videoCodec, audioCodec)


# Original file
fileLocation = sys.argv[1]
videoBitRateModifier = float(sys.argv[2])
audioBitRateModifier = float(sys.argv[3])
outputFile = None
# fileLocation = 'original/system/vendor/etc/media_profiles_vendor.xml'
# bitRateModifier = 1
# outputFile = 'oneplus-6-6t-hevc-recording/system/vendor/etc/media_profiles_vendor.xml'
if len(sys.argv) > 4:
    outputFile = sys.argv[4]
# XML Tree
tree = et.parse(fileLocation)
MediaSettings = tree.getroot()
declarations = editAttributeList(fileLocation)

# Adjust audio codec based on the audiobitrate modifier
audioCodecChoosing = 'heaac'
if audioBitRateModifier > 1.34:
    audioCodecChoosing = 'aac'

changeCodecs(MediaSettings, videoBitRateModifier,
             audioBitRateModifier, audioCodec=audioCodecChoosing)
if outputFile != None:
    mediaProfilesPrint(tree, declarations, outputFile)
else:
    mediaProfilesPrint(tree, declarations)
